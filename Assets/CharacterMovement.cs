using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class CharacterMovement : MonoBehaviour {
    private CharacterController characterController;

    private float horizontalMovement;
    private float verticalMovement;
    private float mouseX;
    private float mouseY;
    public float mouseSensitivity = 100f;

    public Transform cameraTransform;
    public Transform gunTransform;

    private float xRotation;
    private float yRotation;
    [FormerlySerializedAs("movement_speed")] public float movementSpeed;

    private Vector3 moveDirection;
    // Start is called before the first frame update
    void Start() {
        characterController = gameObject.GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
        moveDirection = gameObject.transform.forward * verticalMovement + gameObject.transform.right * horizontalMovement;
        horizontalMovement = Input.GetAxis("Horizontal");
        verticalMovement = Input.GetAxis("Vertical");

        characterController.SimpleMove(moveDirection * movementSpeed);

        mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        cameraTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        

        yRotation += mouseX;
        gameObject.transform.rotation = Quaternion.Euler(0, yRotation, 0);
        gunTransform.rotation = Quaternion.Euler(xRotation, yRotation, 0f);
    }
}