using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMovement : MonoBehaviour {

    [SerializeField] private float movementSpeed = 5f;
    
    // Update is called once per frame
    void Update() {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        
        Vector3 movementDir = new Vector3(horizontalInput, 0f, verticalInput).normalized;
        transform.Translate(movementDir * (movementSpeed * Time.deltaTime));
        
    }
}