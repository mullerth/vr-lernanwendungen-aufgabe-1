using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TargetSpawn : MonoBehaviour {
    public GameObject targetPrefab;

    private GameObject target;
    // Start is called before the first frame update
    void Start() {
        SpawnTarget();
    }


    public void SpawnTarget() {
        Vector3 randomSpawnPoint = new Vector3(Random.Range(-45f, 45f), Random.Range(2f, 10f), Random.Range(-20f, 10f));
        target = Instantiate(targetPrefab, randomSpawnPoint, Quaternion.identity);
    }
}