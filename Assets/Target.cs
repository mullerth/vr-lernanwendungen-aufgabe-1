using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Target : MonoBehaviour {

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Bullet")) {
            Destroy(collision.gameObject);
            transform.position = new Vector3(Random.Range(-45f, 45f), Random.Range(2f, 10f), Random.Range(-20f, 10f));
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.velocity = new Vector3(0, 0, 0);
            Renderer targetRenderer = GetComponent<Renderer>();
            targetRenderer.material.color = Random.ColorHSV();
        }
    }
}